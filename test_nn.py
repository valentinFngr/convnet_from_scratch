from audioop import bias
import unittest
from importlib_metadata import requires
import numpy as np 
import torch 
from modules import * 


class TestConv2D(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestConv2D, self).__init__(*args, **kwargs)
        self.batch = 5
        self.in_width = 16 
        self.in_height = 16 
        self.features_in = 1 
        self.stride = 1
        self.padding = 0
        self.kernel_size = 3
        self.features_out = 10
        output_dim = self._compute_output_dims()
        self.output_dim = output_dim

    def _compute_output_dims(self): 
        """
            Retourne la dimension après convolution
        """
        
        d_width = int(((self.in_width + 2*self.padding - (self.kernel_size - 1) - 1) / self.stride) + 1)
        return d_width

    def test_conv2d_forward(self): 
        
        with torch.no_grad():

            x = np.random.randn(self.batch, self.features_in, self.in_width, self.in_height)
            _x = torch.tensor(x, dtype=torch.float32)

            w = np.random.randn(*(self.features_out, self.features_in, self.kernel_size, self.kernel_size))
            _w = torch.tensor(w, dtype=torch.float32)

            bias = np.ones((self.features_out, ))
            _bias = torch.tensor(bias, dtype=torch.float32)

            conv2d = Conv2D(self.kernel_size, self.features_in, self.features_out, self.stride, self.padding, weights=w, bias=bias)
            oracle_conv2d = torch.nn.Conv2d(self.features_in, self.features_out, self.kernel_size, self.stride, self.padding)
            oracle_conv2d.weight = torch.nn.parameter.Parameter(_w, requires_grad=False)
            oracle_conv2d.bias = torch.nn.parameter.Parameter(_bias, requires_grad=False)

            np.testing.assert_almost_equal(oracle_conv2d.weight.data, w)
            np.testing.assert_almost_equal(oracle_conv2d.bias.data, bias)            
            
            output = conv2d.forward(x)
            oracle_output = oracle_conv2d(_x)
            self.assertEqual(output.shape, oracle_output.shape)

            np.testing.assert_almost_equal(output, oracle_output.numpy(), decimal=4)

    def test_conv2d_backward_delta(self): 

        x = np.random.randn(self.batch, self.features_in, self.in_width, self.in_height)
        _x = torch.tensor(x, dtype=torch.float32, requires_grad=True)

        w = np.random.randn(*(self.features_out, self.features_in, self.kernel_size, self.kernel_size))
        _w = torch.tensor(w, dtype=torch.float32)

        delta = np.ones((self.batch, self.features_out, self.output_dim, self.output_dim))

        conv2d = Conv2D(self.kernel_size, self.features_in, self.features_out, self.stride, self.padding, weights=w, bias=None)
        oracle_conv2d = torch.nn.Conv2d(self.features_in, self.features_out, self.kernel_size, self.stride, self.padding, bias=None)
        oracle_conv2d.weight = torch.nn.parameter.Parameter(_w, requires_grad=False)

        d_delta = conv2d.backward_delta(x, delta)
        oracle_out = oracle_conv2d(_x)
        oracle_out.sum().backward()
        oracle_d_delta = _x.grad
        
        self.assertEqual(d_delta.shape, x.shape)

        np.testing.assert_almost_equal(d_delta, oracle_d_delta, decimal=1)


    def test_conv2d_backward_update_gradient(self): 

        x = np.random.randn(self.batch, self.features_in, self.in_width, self.in_height)
        _x = torch.tensor(x, dtype=torch.float32)

        w = np.random.randn(*(self.features_out, self.features_in, self.kernel_size, self.kernel_size))
        _w = torch.tensor(w, dtype=torch.float32, requires_grad=False)

        delta = np.ones((self.batch, self.features_out, self.output_dim, self.output_dim))

        conv2d = Conv2D(self.kernel_size, self.features_in, self.features_out, self.stride, self.padding, weights=w, bias=None)
        oracle_conv2d = torch.nn.Conv2d(self.features_in, self.features_out, self.kernel_size, self.stride, self.padding, bias=None)
        oracle_conv2d.weight = torch.nn.parameter.Parameter(_w, requires_grad=True)

        d_w = conv2d.backward_update_gradient(x, delta)
        self.assertEqual(d_w.shape, w.shape)

        oracle_out = oracle_conv2d(_x)
        oracle_out.sum().backward()
        
        oracle_d_w = oracle_conv2d.weight.grad
    
        np.testing.assert_almost_equal(d_w, oracle_d_w, decimal=4)



class TestMaxPooling2D(unittest.TestCase): 
    def __init__(self, *args, **kwargs):
        super(TestMaxPooling2D, self).__init__(*args, **kwargs)
        self.batch = 1
        self.in_width = 10
        self.in_height = 10 
        self.features_in = 16        
        self.kernel_size = 2
        self.stride = 2
        self.padding = 0

    def test_forward_max_pooling_2d(self): 

        x = np.random.randn(self.batch, self.features_in, self.in_width, self.in_height)
        _x = torch.tensor(x, dtype=torch.float32, requires_grad=False)

        maxpooling2d = MaxPool2d(self.kernel_size, self.stride, self.padding) 
        oracle_maxpooling2d = torch.nn.MaxPool2d(kernel_size=2, stride=2)

        output = maxpooling2d.forward(x)
        oracle_output = oracle_maxpooling2d(_x)

        np.testing.assert_almost_equal(output, oracle_output, decimal=4)


    def test_backward_delta_max_pooling_2d(self): 
        

        x = np.random.randn(self.batch, self.features_in, self.in_width, self.in_height)
        _x = torch.tensor(x, dtype=torch.float32, requires_grad=True)

        delta = np.ones((self.batch, self.features_in, self.in_width, self.in_height))

        maxpooling2d = MaxPool2d(self.kernel_size, self.stride, self.padding) 
        oracle_maxpooling2d = torch.nn.MaxPool2d(kernel_size=2, stride=2)

        d_z = maxpooling2d.backward_delta(x, delta)
        out = oracle_maxpooling2d(_x)
        out.sum().backward()
        oracle_d_z = _x.grad

        np.testing.assert_almost_equal(d_z, oracle_d_z, decimal=4)





        





if __name__ == '__main__':
    unittest.main()

        


