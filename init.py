import numpy as np


def random_init(shape, scale_factor=1):
    """
        Initialisation aléatoire de nos poids
        Args: 
            shape : un tuple indiquant la shape de nos poids
            scale_factor : multiplie la matrice de poids
        Output: 
            params : les poids initialisés
    """

    params = np.random.randn(*shape) * scale_factor
    return params


def constant_init(shape, constant_value):
    """
        Initialisation constante de nos poids
        Args: 
            shape : un tuple indiquant la shape de nos poids
            constant_value : la constante
        Output: 
            params : les poids initialisés
    """

    assert type(constant_value) == int

    params = np.ones(shape) * constant_value
    return params


def xavier_init_uniform(shape, feature_in, feature_out): 
    """
        Initialisation de xavier
        Args: 
            shape : un tuple indiquant la shape de nos poids
            feature_in  
            feature_out
        Output: 
            params : les poids initialisés
    """

    k = np.sqrt(6/feature_in + feature_out)
    params = np.random.uniform(-k, k)
    return params
    



