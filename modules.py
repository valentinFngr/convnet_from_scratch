from matplotlib.pyplot import axis
import numpy as np
import init

class Module():
	def __init__(self):
		pass

	def forward(self, x):
		pass

	def zero_grad(self):
		pass

	def backward_update_gradient(self, x,delta):
		pass

	def backward_delta(self, x,delta):
		pass

	def update_parameters(self, gradient_step):
		pass

class Loss():
	def __init__(self):
		pass

	def forward(self, y,yhat):
		pass

	def backward(self, y,yhat):
		pass



def zero_padding(x, padding):
    x = np.pad(x, ((0, 0), (0, 0), (padding, padding), (padding, padding)), 'constant')
    return x

class Conv2D(Module): 

    """
        Une class implémentant une couche de convolution
    """
    
    def __init__(self, kernel_size, features_in, features_out, stride=1, padding=0, weights="random", bias=None): 
        """
            kernel_size : la taille du kernel (kernel_size, kernel_size)
            features_in : nombre de filtres entrant 
            features_out : nombre de filtres sortant 
            stride : décalage dans le calcul de la convolution 
            padding : bord
        """
        self.kernel_size = kernel_size
        self.features_in = features_in
        self.features_out = features_out
        self.stride = stride 
        self.padding = padding

        self.gradient = self.zero_grad()

        weights_shape = (features_out, features_in, kernel_size, kernel_size)
        bias_shape = (self.features_out, )

        if weights is not None: 
            # si on fournit des poids
            if type(weights) != str: 
                self.weights = weights
            elif weights == "random": 
                self.weights = init.random_init(weights_shape)
            elif weights == "zeros": 
                self.weights = init.constant_init(weights_shape, 0)
            elif weights == "xavier": 
                self.weights = init.xavier_init_uniform(weights_shape, features_in, features_out)
                
        if bias is not None: 
            self.bias = bias
        elif bias == "zeros": 
            self.bias = init.constant_init(bias_shape, 0)
        else: 
            self.bias = None

    def zero_grad(self):
        # todo : add bias
        self.weights = np.zeros_like(self.weights)
        

    def _compute_output_dims(self, in_width, kernel_size, stride, padding): 
        """
            Retourne la dimension après convolution
        """
        
        d_width = int(((in_width + 2*padding - (kernel_size - 1) - 1) / stride) + 1)
        return d_width

    def forward(self, x): 
        """
            Implémentation d'une passe vers l'avant pour un réseau convolutif
            Args: 
                x : (batch_size, feature_in, width, height)
            Output: 
                (batch_size, feature_out, width, height)
        """        

        if self.padding is not None: 
            x = zero_padding(x, self.padding)

        batch_size = x.shape[0]

        x_width = x.shape[2]
        x_height = x.shape[3] 

        # ils sont identiques car on introduit des kernels simples (3x3) ou (7x7) par ex
        d_width = int(((x_width + 2*self.padding - (self.kernel_size - 1) - 1) / self.stride) + 1)
        d_height = int(((x_height + 2*self.padding - (self.kernel_size - 1) - 1) / self.stride) + 1)

        print(d_width, d_height)

        output = np.zeros((batch_size, self.features_out, d_width, d_height))

        d = self.kernel_size // 2

        for i in range(d, x_width - d): 
            for j in range(d, x_height - d):                 
                window = x[:, None, :, i-d:i+d+1, j-d:j+d+1] * self.weights[None, :, :, :, :] 
                output[:, :, i-1, j-1] = np.sum(window, axis=(2, 3, 4))

        if self.bias is None: 
            return output        
        return output + np.expand_dims(self.bias, axis=(0, 2, 3))


    def backward_delta(self, x, delta):
        """
            détermine la dérivée partiellement à l'entrée de la couche
            Rappel : weights_shape : (features_out, features_in, kernel_size, kernel_size)
            Args: 
            
                x : (batch_size, feature_in, width, height)
                delta : (batch_size, feature_out, width, height)
            Output: 
                (batch_size, feature_out, width, height)

        """

        d_delta = np.zeros_like(x)

        temp_x = np.transpose(self.weights, (1, 0, 2, 3))
        temp_kernel = delta
        
        padding = temp_kernel.shape[3] - 1
        temp_x = zero_padding(temp_x, padding)
        x_width = temp_x.shape[-1]
        temp_kernel_size = temp_kernel.shape[3]

        for i in range(x_width - temp_kernel_size + 1): 
            for j in range(x_width - temp_kernel_size + 1):  
                start_row = i 
                start_col = j
                end_row = start_row + temp_kernel_size 
                end_col = start_col + temp_kernel_size
                window = temp_x[None,:,:, start_row:end_row, start_col:end_col] * temp_kernel[:,None, :, :, :] 
                d_delta[:, :, i, j] = np.sum(window, axis=(2, 3, 4))
            
        return d_delta 

    def backward_update_gradient(self, x, delta):
        """
            Détermine la dérivée partiellement aux paramètres du module
            Rappel : weights_shape : (features_out, features_in, kernel_size, kernel_size)
            Args: 
            
                x : (batch_size, feature_in, width, height)
                delta : (batch_size, feature_out, width, height)
            Output: 
                (features_out, features_in, kernel_size, kernel_size)

        """
        # initialization
        d_w = np.zeros_like(self.weights)

        temp_x = x 
        temp_kernel = delta 
        x_width = x.shape[-1]

        temp_x = np.transpose(temp_x, (1, 0, 2, 3))
        temp_kernel = np.transpose(temp_kernel, (1, 0, 2, 3))

        temp_kernel_size = temp_kernel.shape[-1]

        print(temp_kernel.shape)
        print(temp_x.shape)

        x_width, x_height = temp_x.shape[-2], temp_x.shape[-1]
        
        for i in range(x_width - temp_kernel_size+1): 
            for j in range(x_height - temp_kernel_size+1): 
                start_row = i 
                start_col = j
                end_row = start_row + temp_kernel_size 
                end_col = start_col + temp_kernel_size 
                window = temp_x[None, :, :, start_row:end_row, start_col:end_col] * temp_kernel[:, None, :, :,:]
                d_w[:,:, i, j] = np.sum(window, axis=(2, 3, 4))
        self.gradient += d_w
        return d_w


    def update_parameters(self, gradient_step): 
        """
            update the weights
        """
        self.weights -= gradient_step * self.gradient        


class MaxPool2d(Module): 
    """
    NOTE : TOUTES LES CONVOLUTIONS SONT SUPPOSEES EFFECTUEES SUR DES IMAGES CARREES !!
        Implémentation d'une couche de max pooling. 
        Chaque patch du pooling retourne sa valeur maximum
        
    """

    def __init__(self, kernel_size=2, stride=2, padding=0): 
        super(MaxPool2d, self).__init__()
        self.kernel_size = kernel_size 
        self.stride = stride 
        self.padding = padding 

    def forward(self, x): 
        """
            x : (batch_size, feature_in, width, height)
            delta : (batch_size, feature_in, width//2, height//2)
        """

        x_width = x.shape[-1]
        output = np.zeros((x.shape[0], x.shape[1], int(x_width // 2), int(x_width // 2)))
        assert output.shape[-1] == (x_width // 2)
        for i_enum, i in enumerate(range(0, x_width - self.kernel_size + 1, self.stride)): 
            for j_enum, j in enumerate(range(0, x_width - self.kernel_size + 1, self.stride)): 
                start_row = i 
                end_row = i + self.kernel_size
                start_col = j 
                end_col = j + self.kernel_size
                window = x[:, :, start_row:end_row, start_col:end_col]
                output[:, :, i_enum, j_enum] = window.max(axis=(2, 3))

        return output 

    def backward_delta(self, x, delta):
        """
            Dérivée partiellement à l'entrée du module pour MaxPool2d
        """
        d_z = np.zeros_like(x)

        x_width = x.shape[-1]
        channels  = x.shape[1] 

        for n in range(x.shape[0]): 
            for c in range(channels):
                for i in range(0, x_width - self.kernel_size + 1, self.stride): 
                    for j in range(0, x_width - self.kernel_size + 1, self.stride): 
                            start_row = i 
                            end_row = i + self.kernel_size
                            start_col = j 
                            end_col = j + self.kernel_size
                            window = x[n,c,start_row:end_row, start_col:end_col] # (1, 1, 2, 2)
                            d_z[n,c,start_row:end_row,start_col:end_col] = np.where(window == np.max(window), 1, 0)

        return d_z * delta
